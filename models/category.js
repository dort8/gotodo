//Category Schema
const mongoose = require("mongoose");

const categorySchema = new mongoose.Schema({
  ParentId: {
    type: String,
  },
  title: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  editedAt: {
    type: Date,
  },
  deletedAt: {
    type: Date,
  },
});
//TODO: Add a Category Icon
//TODO: Add Connection between Category and ToDo
module.exports = mongoose.model("category", categorySchema);
