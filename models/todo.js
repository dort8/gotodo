//Todo Schema
const mongoose = require("mongoose");

const todoSchema = new mongoose.Schema({
  dependentId: {
    type: String,
  },
  title: {
    type: String,
    required: true,
  },
  todo: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  doneAt: {
    type: Date,
  },
  editedAt: {
    type: Date,
  },
  deletedAt: {
    type: Date,
  },
  isDone: {
    type: Boolean,
    default: false,
  },
  config: {
    type: Object,
  },
  priority: {
    id: Number,
    levelLabel: String,
  },
});
//TODO: Add Connection between Category and ToDo
//TODO: Add Connection between Hashtag and ToDo

module.exports = mongoose.model("todo", todoSchema);
