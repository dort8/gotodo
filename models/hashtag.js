//Hashtag Schema
const mongoose = require("mongoose");

const hashtagSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  editedAt: {
    type: Date,
  },
  deletedAt: {
    type: Date,
  },
});

//TODO: Add Connection between Hashtag and ToDo
module.exports = mongoose.model("hashtag", hashtagSchema);
