const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();

const app = express();
const dbConnection = mongoose.connection;
const todoRouter = require("./routers/todos");
const categoryRouter = require("./routers/category");
const hashtagRouter = require("./routers/hashtag");

//PORT
app.listen(process.env.PORT);

mongoose.connect(process.env.DB_URL);

//Open DB connection
dbConnection.on("open", () => {
  console.log("Connection is Done");
});

//Let express accept JSON as recived data
app.use(express.json());

//API End Point
app.use("/todos", todoRouter);

app.use("/categories", categoryRouter);

app.use("/hashtags", hashtagRouter);
