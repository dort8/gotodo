const express = require("express");
const router = express.Router();
const Todo = require("../models/todo");

//Get All ToDo's
router.get("/", async (req, res) => {
  try {
    const todos = await Todo.find();
    res.json(todos);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Get Single ToDo
router.get("/:id", async (req, res) => {
  try {
    const singleTodo = await Todo.findById(req.params.id);
    res.json(singleTodo);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Post single ToDo
router.post("/", async (req, res) => {
  const todoVal = new Todo({
    dependentId: req.body.dependentId,
    title: req.body.title,
    todo: req.body.todo,
    createdAt: req.body.createdAt,
    doneAt: req.body.doneAt,
    editedAt: req.body.editedAt,
    deletedAt: req.body.deletedAt,
    isDone: req.body.isDone,
    config: req.body.config,
    priority: req.body.priority,
  });
  try {
    const data = await todoVal.save();
    res.json(data);
  } catch (err) {
    res.send("Opss" + err);
  }
});

//Edit Single Todo
router.patch("/:id", async (req, res) => {
  //TODO: Check only the requested Changes and reassign the needed one only
  try {
    const singleTodo = await Todo.findById(req.params.id);
    singleTodo.dependentId = req.body.dependentId;
    singleTodo.title = req.body.title;
    singleTodo.todo = req.body.todo;
    singleTodo.createdAt = req.body.createdAt;
    singleTodo.doneAt = req.body.doneAt;
    singleTodo.editedAt = req.body.editedAt;
    singleTodo.deletedAt = req.body.deletedAt;
    singleTodo.isDone = req.body.isDone;
    singleTodo.config = req.body.config;
    singleTodo.priority = req.body.priority;

    const data = await singleTodo.save();
    res.json(data);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Delete Single ToDo
router.delete("/:id", async (req, res) => {
  try {
    const singleTodo = await Todo.findById(req.params.id);
    const data = await singleTodo.delete();
    res.json(data);
  } catch (err) {
    res.send("Opss " + err);
  }
});

module.exports = router;
