const express = require("express");
const router = express.Router();
const Hashtag = require("../models/hashtag");

//Get All Categoreis
router.get("/", async (req, res) => {
  try {
    const hashtags = await Hashtag.find();
    res.json(hashtags);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Get Single Hashtag
router.get("/:id", async (req, res) => {
  try {
    const singleHashtag = await Hashtag.findById(req.params.id);
    res.json(singleHashtag);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Post single Hashtag
router.post("/", async (req, res) => {
  const hashtagVal = new Hashtag({
    title: req.body.title,
    createdAt: req.body.createdAt,
    editedAt: req.body.editedAt,
    deletedAt: req.body.deletedAt,
    todo: req.body.todo,
  });
  try {
    const data = await hashtagVal.save();
    res.json(data);
  } catch (err) {
    res.send("Opss" + err);
  }
});

//Edit Single Hashtag
router.patch("/:id", async (req, res) => {
  //TODO: Check only the requested Changes and reassign the needed one only
  try {
    const singleHashtag = await Hashtag.findById(req.params.id);
    singleHashtag.ParentId = req.body.ParentId;
    singleHashtag.title = req.body.title;
    singleHashtag.createdAt = req.body.createdAt;
    singleHashtag.editedAt = req.body.editedAt;
    singleHashtag.deletedAt = req.body.deletedAt;
    singleHashtag.todo = req.body.todo;

    const data = await singleHashtag.save();
    res.json(data);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Delete Single Hashtag
router.delete("/:id", async (req, res) => {
  try {
    const singleHashtag = await Hashtag.findById(req.params.id);
    const data = await singleHashtag.delete();
    res.json(data);
  } catch (err) {
    res.send("Opss " + err);
  }
});

module.exports = router;
