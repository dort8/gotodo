const express = require("express");
const router = express.Router();
const Category = require("../models/category");

//Get All Categoreis
router.get("/", async (req, res) => {
  try {
    const categories = await Category.find();
    res.json(categories);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Get Single Category
router.get("/:id", async (req, res) => {
  try {
    const singleCategory = await Category.findById(req.params.id);
    res.json(singleCategory);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Post single ToDo
router.post("/", async (req, res) => {
  const CategoryVal = new Category({
    ParentId: req.body.ParentId,
    title: req.body.title,
    createdAt: req.body.createdAt,
    editedAt: req.body.editedAt,
    deletedAt: req.body.deletedAt,
    todo: req.body.todo,
  });
  try {
    const data = await CategoryVal.save();
    res.json(data);
  } catch (err) {
    res.send("Opss" + err);
  }
});

//Edit Single Category
router.patch("/:id", async (req, res) => {
  //TODO: Check only the requested Changes and reassign the needed one only
  try {
    const singleCategory = await Category.findById(req.params.id);
    singleCategory.ParentId = req.body.ParentId;
    singleCategory.title = req.body.title;
    singleCategory.createdAt = req.body.createdAt;
    singleCategory.editedAt = req.body.editedAt;
    singleCategory.deletedAt = req.body.deletedAt;
    singleCategory.todo = req.body.todo;

    const data = await singleCategory.save();
    res.json(data);
  } catch (err) {
    res.send("Opss " + err);
  }
});

//Delete Single ToDo
router.delete("/:id", async (req, res) => {
  try {
    const singleCategory = await Category.findById(req.params.id);
    const data = await singleCategory.delete();
    res.json(data);
  } catch (err) {
    res.send("Opss " + err);
  }
});

module.exports = router;
